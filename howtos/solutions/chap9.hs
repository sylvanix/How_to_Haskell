
import Data.Char (toLower)


-- Q9.1  Use filter and length to re-create the elem function.
{-
TRYING OUT SOME IDEAS IN GHCi ...
ghci > alist
[1,2,3]
ghci > filter (\x-> x==2) alist
[2]
ghci > length $ filter (\x-> x==2) alist
1
-}

myElem :: Eq a => a -> [a] -> Bool
myElem a [] = False
myElem a (x:xs) = if (length $ filter (\x-> x==a) (x:xs)) > 0
                  then True
                  else False

{-
TESTING THE FUNCTION:
ghci > :l myelem.hs 
[1 of 1] Compiling Main             ( myelem.hs, interpreted )
Ok, modules loaded: Main.
ghci > alist = 1:2:3:[]
ghci > myElem 2 alist
True
-}

-- *********************************************************************

-- Q9.2  Your isPalindrome function from lesson 6 doesn't handle sentences
--       with spaces or capitals. Use map and filter to make sure the phrase
--       "A man a plan a canal Panama" is recognized as a palindrome.

{-
TRYING OUT SOME IDEAS IN GHCi ...
from chapter 6:
ghci > isPal word = word == reverse word
ghci > :t isPal
isPal :: Eq a => [a] -> Bool
ghci > isPal "hatah"
True
ghci > isPal "hat ah"
False

now, filter the space characters:
ghci > filter (\x-> x/=' ') "hat ah"
"hatah"

handle the capitals with:
ghci > import Data.Char
ghci > map toLower "haTah"
-}

isPal word = newword  == reverse newword
    where newword = filter (\x-> x/=' ') (map toLower word)

{-
ghci > :l myelem.hs 
[1 of 1] Compiling Main             ( myelem.hs, interpreted )
Ok, modules loaded: Main.
ghci > isPal "A man a plan a canal Panama"
True
-}



-- *********************************************************************

-- Q9.3  In mathematics, the harmonics series is the sume of 1/1 + 1/2 +
--       1/3 + 1/4 ... Write a function harmonic that takes an argument n
--       and calculates the sum of the series to n. Make sure to use lazy
--       evaluation.
{-
TRYING OUT SOME IDEAS IN GHCi ...
ghci > sum $ map (\x-> 1/x) [1 .. 100]
5.187377517639621
-}

harmonic 0 = 0
harmonic n = sum $ map (\x-> 1/x) [1 .. n]

{-
ghci > harmonic 100
5.187377517639621
-}

-- I like this implementation:
-- harmonic n = foldr (+) (0) (map (1/) [1..n])
-- (source: https://github.com/jjunqueira/get-programming-with-haskell/blob/master/src/Chapter09.hs)

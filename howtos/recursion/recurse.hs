
myGCD a b = if remainder == 0
            then b
            else myGCD b remainder
    where remainder = a `mod` b

{-
a recursive function implementing Euclid's algorithm for finding the greatest common divisor or two
integers.
source: Get Programming with Haskell, by Will Kurt, chapter 7.

-}
-- *********************************************************************************

myTake _ [] = []
myTake 0 _ = []
myTake n (x:xs) = x:rest
    where rest = myTake (n - 1) xs

{-
a recursive implementation of the take function.
source: Get Programming with Haskell, by Will Kurt, chapter 8.
-}
-- *********************************************************************************

myCycle (first:rest) = first:myCycle (rest ++ [first])

{-
a recursive implementation of the cycle function.
source: Get Programming with Haskell, by Will Kurt, chapter 8.

test like so:
  ghci > take 10 (myCycle [1,2,3])
-}
-- *********************************************************************************

ackermann 0 n = n + 1
ackermann m 0 = ackermann (m-1) 1
ackermann m n = ackermann (m-1) (ackermann m (n-1))

{-
Implement the Ackermann function in Haskell. The Ackermann function follows these three rules:
* if m = 0, return n+1.
* if n = 0, return A(m-1, 1).
* if both m != 0 and n != 0, then A( m-1, A(m, n-1) ).

Time the function calls using  :set +s
  ghci > :set +s
  ghci > ackermann 3 3
61
(0.01 secs, 912,920 bytes)
  ghci > ackermann 3 8
2045
(1.45 secs, 1,018,393,064 bytes)
  ghci > ackermann 3 9
4093
(5.82 secs, 4,084,480,960 bytes)

source: Get Programming with Haskell, by Will Kurt, chapter 8.

-}
-- *********************************************************************************

collatz 1 = 1
collatz n = if even n
            then 1 + collatz (n `div` 2)
            else 1 + collatz (n*3 + 1)

{-
Implement a function to calculate the Collatz function length of an integer (the number of times
the function recurses before terminating at 1)
source: Get Programming with Haskell, by Will Kurt, chapter 8.

  ghci > collatz 9
20
  ghci > collatz 91
93
  ghci > map collatz [100 .. 120]
[26,26,26,88,13,39,13,101,114,114,114,70,21,13,34,34,21,21,34,34,21]

-}
-- *********************************************************************************

fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

{-
The above function for calculating Fibonacci numbers is the most straightforward definition, but
this implementation is inefficient, like the Ackermann function, because of the mutually recursive
calls. Below is a much more efficient way to compute the n-th Fibonacci number.

-}
-- *********************************************************************************

-- choosing the list head to be the pivot element
quicksort :: (Ord a) => [a] -> [a]  
quicksort [] = []  
quicksort (x:xs) = smallers ++ [x] ++ largers
                   where smallers = quicksort [a | a <- xs, a <= x]
                         largers = quicksort [a | a <- xs, a > x]
{-
quicksort (using recursion)
-}                     

-- *********************************************************************************
-- *********************************************************************************
-- *********************************************************************************

{-
Implement some of the prelude functions using recrusion
-}

repeat' :: a -> [a]  
repeat' x = x:repeat' x


replicate' :: (Num i, Ord i) => i -> a -> [a]  
replicate' n x  
    | n <= 0    = []  
    | otherwise = x:replicate' (n-1) x


take' :: (Num i, Ord i) => i -> [a] -> [a]  
take' n _  
    | n <= 0   = []  
take' _ []     = []  
take' n (x:xs) = x : take' (n-1) xs


zip' :: [a] -> [b] -> [(a,b)]  
zip' _ [] = []  
zip' [] _ = []  
zip' (x:xs) (y:ys) = (x,y):zip' xs ys


elem' :: (Eq a) => a -> [a] -> Bool  
elem' a [] = False  
elem' a (x:xs)  
    | a == x    = True  
    | otherwise = a `elem'` xs
    
    
maximum' :: (Ord a) => [a] -> a  
maximum' [] = error "maximum of empty list"  
maximum' [x] = x  
maximum' (x:xs)   
    | x > maxTail = x  
    | otherwise = maxTail  
    where maxTail = maximum' xs
    
    
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]  
zipWith' _ [] _ = []  
zipWith' _ _ [] = []  
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys



map' :: (a -> b) -> [a] -> [b]  
map' _ [] = []  
map' f (x:xs) = f x : map' f xs



filter' :: (a -> Bool) -> [a] -> [a]  
filter' _ [] = []  
filter' p (x:xs)   
    | p x       = x : filter' p xs  
    | otherwise = filter' p xs    






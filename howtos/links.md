### Haskell Links:

##### [GHC Docs](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/)  


##### [99 Haskell Problems](https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems)  


##### [Euler Problems](https://wiki.haskell.org/Euler_problems)  


##### [Haskell Wiki](https://wiki.haskell.org/Haskell)  






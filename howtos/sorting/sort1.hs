import Data.List


compareLastNames name1 name2 = if lastName1 > lastName2
                               then GT
                               else if lastName1 < lastName2
                                    then LT
                                    else if firstName1 > firstName2
                                         then GT
                                         else if firstName1 < firstName2
                                              then LT
                                              else EQ
    where lastName1 = snd name1
          lastName2 = snd name2
          firstName1 = fst name1
          firstName2 = fst name2

{-
source: Get Programming with Haskell, by Will Kurt, chapter 4

in ghci, define names, for example:
  ghci > :l sort1.hs
  ghci > names = [("Ian", "Curtis"),("Bernard","Sumner"),("Peter", "Hook"),("Stephen","Morris")]
then using the comarator function defined above with sortBy imported from Data.List:
  ghci > sortBy compareLastNames names
[("Ian","Curtis"),("Peter","Hook"),("Stephen","Morris"),("Bernard","Sumner")]
-}

-- ###########################################################################################

compareAges person1 person2 = if age1 > age2
                              then GT
                              else if age1 < age2
                                   then LT
                                   else EQ
    where age1 = snd person1
          age2 = snd person2

{-
testing for this comparator:
  ghci > alist = [("delvan", 34), ("Aquisha", 23), ("MartinX", 31), ("TerrenceS", 26), ("Reynold", 41), ("Bischerelle", 55)]
  ghci > sortBy compareAges alist
[("Aquisha",23),("TerrenceS",26),("MartinX",31),("delvan",34),("Reynold",41),("Bischerelle",55)]
-}

-- ###########################################################################################

compareAges2 person1 person2 = compare age1 age2
    where age1 = snd person1
          age2 = snd person2
{-
The modified compareAges function above uses Haskell's built-in compare function which returns
either GT, LT, or EQ.
-}

-- ###########################################################################################

-- Quicksort
qsort [] = []
qsort (a:as) = qsort left ++ [a] ++ qsort right
  where (left,right) = (filter (<=a) as, filter (>a) as)

main = print (qsort [8, 4, 0, 3, 1, 23, 11, 18])


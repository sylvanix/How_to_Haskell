import Data.List (group, sort)



-- A function that counts all pairs in a list of Ord and returns the sum

countPairs :: Ord a => [a] -> Int
countPairs xs = sum $ map (\xs -> length xs `div` 2) (group $ sort xs)

{-
ghci > :l pairs.hs 
[1 of 1] Compiling Main             ( pairs.hs, interpreted )
Ok, modules loaded: Main.
ghci > clist = [10,20,20,10,10,30,50,10,20]
ghci > countPairs clist
3
-}

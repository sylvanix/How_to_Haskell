### Data Containers  

Something about containers in general, then about containers in Haskell...


[**Data.HashMap**](https://hackage.haskell.org/package/containers-0.6.5.1/docs/Data-Map-Lazy.html)  

```
λ: import Prelude hiding (null, lookup, map, filter)
λ: import Data.Map.Lazy
λ: hashMap = fromList [(1 :: Int, 'a'), (2, 'b'), (3, 'c')]
λ: hashMap
fromList [(1,'a'),(2,'b'),(3,'c')]
λ: keys hashMap
[1,2,3]
λ: elems hashMap
"abc"
λ: null hashMap
False
λ: size hashMap
3
λ: member 2 hashMap
True
λ: lookup 2 hashMap
Just 'b'
λ: lookup 4 hashMap
Nothing
λ: hashMap ! 1
'a'

λ: newMap = insert 4 'd' hashMap
λ: newMap
fromList [(1,'a'),(2,'b'),(3,'c'),(4,'d')]

λ: anotherMap = delete 2 newMap
λ: anotherMap
fromList [(1,'a'),(3,'c'),(4,'d')]

```

Another ...

```

```





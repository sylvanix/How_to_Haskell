import Data.List (sort, group)

-- replace
--   replace the value of the <n>th element of a <list> with a value <a>
replace :: (Ord t, Num t) => [t1] -> (t, t1) -> [t1]
replace [] _ = []
replace (_:xs) (0,a) = a:xs
replace (x:xs) (n,a)
    | n < 0     = (x:xs)
    | otherwise = x: replace xs (n-1,a)


-- uniquify
--   returns the sorted, unique elements of a <list>
{-
   note: one could use nub from Data.List which preserves the original list order
   ghci > a = [4,5,6,3,4,2,3,9,6,5,5,1,9]
   ghci > import Data.List (nub)
   ghci > nub a
   [4,5,6,3,2,9,1]
-}
uniquify :: Ord t => [t] -> [t]
uniquify [] = []
uniquify a = map head $ group $ sort a


--multByAdd
--   returns the multiplication of <a> * <n>
multByAdd _ 0 = 0
multByAdd a n = a + multByAdd a (n - 1)


-- expon
--   returns the exponentiation of <a> ^ <n>
expon _ 0 = 1
expon a n  = a * expon a (n - 1)


-- fibStd
--   the standard definition (https://wiki.haskell.org/The_Fibonacci_sequence)
fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)


-- fibsZw
--   canonical zipWith implementation (infinite list)
fibs = 1 : 1 : zipWith (+) fibs (tail fibs)


-- fastFib
--   a logarithmic time implementation. Return (fib n, fib (n + 1))
fastFib = fst . fib2
fib2 0 = (1, 1)
fib2 1 = (1, 2)
fib2 n
 | even n    = (a*a + b*b, c*c - a*a)
 | otherwise = (c*c - a*a, b*b + c*c)
 where (a,b) = fib2 (n `div` 2 - 1)
       c     = a + b


-- replTup
--   in the tuple (a,b), where a is an Int, replicate b a times.
{- Example of usage:
  ghci > map replTup $ zip [1,2,3] ['a','b','c']
  ["a","bb","ccc"]
  ghci > concat $ map replTup $ zip [1,2,3] ['a', 'b','c']
  "abbccc"
-}
replTup :: (Int, a) -> [a]
replTup (a,b) = (\x -> replicate (fst x) (snd x)) (a,b)


-- dropEvery
--   remove every n-th element from the list
dropEvery :: Int -> [a] -> [a]
dropEvery n xs
  | length xs < n = xs
  | otherwise     = take (n-1) xs ++ dropEvery n (drop n xs)



-- rotate
--   rotate a list <y> places to the left
rotate :: [a] -> Int -> [a]
rotate [] _ = []
rotate x 0 = x
rotate x y
  | y > 0 = rotate (tail x ++ [head x]) (y-1)
  | otherwise = rotate (last x : init x) (y+1)


-- removeAt
--   remove the <n>th element (starting from 0) from a list
removeAt n xs
    | length xs < n = xs
    | otherwise     = xs' ++ tail xs''
        where (xs',xs'') = splitAt n xs


-- insertAt
--   insert <a> in the <n>th position of a list
insertAt a n xs
    | length xs < n = xs
    | otherwise     = xs' ++ [a] ++ xs''
        where (xs',xs'') = splitAt n xs

{-
The canonical pure functional way to do fib
source: https://www.reddit.com/r/haskell/comments/xeiic/haskell_and_fibonacci_numbers_beginner/

EXCERPT
To add to that, this problem has nothing to do with the language. You get the same problem with Haskell, Scheme, Java, etc. The general solution to this problem is to use a technique known as dynamic programming where you save the intermediate results from previous calculations.

In Haskell, the canonical pure functional way to do fib without recalculating everything is:

fib n = fibs!!n
    where fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

Zipping a list with itself is a common pattern in Haskell. The reason this works is laziness. Basically you are defining the infinite list of all fibonacci numbers and using !! to get the nth element. The function zipWith allows to combine 2 lists using a function. In this case we add each element of fibs with the previous one to generate the next one. Intuitively, you can see how that produces the Fibonacci sequence. If you write down the call graph, you will see how that works for small values of n.


-}

fib n = fibs !! n
    where fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

{-
writing down the list for successive calls of fib:
n = 0         0 1 : 1
n = 1         0 1 : 1 2
n = 2         0 1 : 1 2 3
n = 3         0 1 : 1 2 3 5
n = 4         0 1 : 1 2 3 5 8
n = 5         0 1 : 1 2 3 5 8 13

The list grows by one element with each call to fib; there is no recalculating and no mutually
recursive calls, so even a large number like fib(100) is computed very quickly.

ghci > fib 100
354224848179261915075
 
note, the above implementation is also found at https://wiki.haskell.org/The_Fibonacci_sequence#Canonical_zipWith_implementation
-}

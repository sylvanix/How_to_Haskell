import Data.List   -- for the alternate replace method 

-- First method

replaceAtWith [] _ = []
replaceAtWith (x:xs) (0,val) = val:xs
replaceAtWith (x:xs) (idx,val) =
    if idx < 0
        then (x:xs)
        else x : replaceAtWith xs (idx-1,val)

-- modified version of implementation found here: https://www.reddit.com/r/haskell/comments/8jui5k/how_to_replace_an_element_at_an_index_in_a_list/ 

{-
ghci > :l repaw.hs 
[1 of 1] Compiling Main             ( repaw.hs, interpreted )
Ok, modules loaded: Main.
ghci > alist = 1:2:3:4:5:6:7:[]
ghci > alist
[1,2,3,4,5,6,7]
ghci > replaceAtWith alist (2,9)
[1,2,9,4,5,6,7]
ghci > 
-}
-- ########################################################################

-- Alternate method, using splitAt, from same source as above

replace :: [a] -> Int -> a -> [a]
replace xs i e = case splitAt i xs of
   (before, _:after) -> before ++ e: after
   _ -> xs

{-
  ghci > replace alist 2 9
  [1,2,9,4,5,6,7]
  ghci > 
-}

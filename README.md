# How to Haskell  
lest I forget  

[//]: # (Image References)
[image1]: ./images/haskell_logo.png


![alt text][image1]  

## Why program in Haskell?  

I couldn't say it better than Will Kurt did at the beginning of his book [Get Programming in Haskell](https://www.manning.com/books/get-programming-with-haskell):

*"...learning Haskell will make you a better programmer in general..."*  
and  
*"...it provides a crash course in understanding programming language theory. You can't learn enough Haskell to write nontrivial programs and not come away knowing a fair bit about functional programming, lazy evaluation, and sophisticated type systems."*  

What I can say is that if creating a clean, efficient, correct C program feels something like master craftsmanship, then producing the equivalent program in Haskell feels a bit like wizardry.  

The programs and code examples below are meant to serve as reminders to me as I learn Haskell.  

##### [useful Haskell links](./howtos/links.md)  


### How To X in Haskell: 

##### [List Comprehensions](./howtos/list_comp/list_comprehensions.md)  

##### [Sorting](./howtos/sorting)  

##### [Getting Comfortable with Recursion](./howtos/recursion)  

##### [Fibonacci - Dynamic Programming for Efficiency](./howtos/dynamic_programming)  

##### [Replacing Elements in a List by Index](./howtos/replacing_in_list)  

##### [Containers](./howtos/containers/containers.md)  

##### [A baby customized prequel (more like an embryo really)](./howtos/baby_prequel)  

##### [Using the GHCi Debugger](./howtos/ghci)  

##### [Using GHC](./howtos/ghc/ghc.md)  

##### [Using Stack](./howtos/stack/stack.md)  

##### [Solutions to problems in Get Programming with Haskell](./howtos/solutions)  

##### [Interview problems](./howtos/problems/interview_problems.md)  



